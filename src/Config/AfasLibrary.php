<?php

return [
    'debug' => env('AFAS_ENV_DEBUG', false),
    'env'   => env('AFAS_ENV_NUMBER', ''),
    'token' => env('AFAS_ENV_TOKEN', ''),
];
