<?php

namespace improvit\AFASlibrary;

use Illuminate\Support\ServiceProvider;

class AfasLibraryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('improvit\AFASlibrary\AfasController');
        $this->mergeConfigFrom( __DIR__.'/Config/AfasLibrary.php', 'AfasLibrary');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/Config/AfasLibrary.php' => config_path('AfasLibrary.php'),
        ], 'config');
    }
}
