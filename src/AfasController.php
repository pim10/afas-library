<?php

namespace improvit\AFASlibrary;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class AfasController extends Controller
{
    private $client;
    private $options;

    private function setupConnection()
    {
        $this->client = new Client(['base_uri' => 'https://' . config('AfasLibary.env') . '.rest' . (config('AfasLibary.debug') ? 'test' : '' . '.afas.online/profitrestservices/')]);
        $this->options = [
            'headers' => [
                'Authorization' => 'AfasToken ' . base64_encode(config('AfasLibary.token'))
            ]
        ];
    }

    /**
     * @param string $connector
     * @param string $parameters
     * @param string $skip
     * @param string $take
     * @return array
     */
    public function executeWithSkip($connector, $skip, $take, $parameters = '')
    {
        $url      = 'connectors/' . $connector . '?skip=' . $skip . '&take=' . $take . $parameters;
        $response = $this->client->get($url, $this->options);
        $result   = json_decode($response->getBody());

        return $result;
    }

    /**
     * @param $content
     * @param $connectorId
     * @param $process
     * @return mixed
     * @throws \Exception
     */
    public function postData($content, $connector)
    {
        if (is_object($content)) {
            $content = json_encode($content);
        }
        $options         = $this->options;
        $options['body'] = $content;
        try {
            $response = $this->client->post('connectors/' . $connector, $options);
            return json_decode($response->getBody()->getContents());
        } catch (RequestException $e) {
            $error = $e->getResponse()->getBody()->getContents();
            throw new \Exception(json_decode($error)->externalMessage);
        }
    }

    /**
     * @param $content
     * @param $connectorId
     * @param $process
     * @return mixed
     * @throws \Exception
     */
    public function putData($content, $connector)
    {
        if (is_object($content)) {
            $content = json_encode($content);
        }
        $options         = $this->options;
        $options['body'] = $content;
        try {
            $response = $this->client->post('connectors/' . $connector, $options);
            return json_decode($response->getBody()->getContents());
        } catch (RequestException $e) {
            $error = $e->getResponse()->getBody()->getContents();
            throw new \Exception(json_decode($error)->externalMessage);
        }
    }
}
